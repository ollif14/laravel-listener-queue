<?php

namespace App\Listeners;

use App\Events\BlogReviewEditorEvent;
use App\Mail\BlogReviewEditor;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Mail;

class SendEmailEditorBlogReview
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BlogReviewEditorEvent  $event
     * @return void
     */
    public function handle(BlogReviewEditorEvent $event)
    {
        Mail::to($event->user)->send(new BlogReviewEditor($event->user));
    }
}
