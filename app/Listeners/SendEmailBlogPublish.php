<?php

namespace App\Listeners;

use App\Events\BlogPublishEvent;
use App\Mail\BlogPublish;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Mail;

class SendEmailBlogPublish
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BlogPublishEvent  $event
     * @return void
     */
    public function handle(BlogPublishEvent $event)
    {
        Mail::to($event->blog)->send(new BlogPublish($event->blog));
    }
}
