<?php

namespace App\Listeners;

use App\Events\BlogReviewpembuatEvent;
use App\Mail\BlogReviewPembuat;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Mail;

class SendEmailPembuatBlogReview
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BlogReviewPembuatEvent  $event
     * @return void
     */
    public function handle(BlogReviewPembuatEvent $event)
    {
        Mail::to($event->blog)->send(new BlogReviewPembuat($event->blog));
    }
}
