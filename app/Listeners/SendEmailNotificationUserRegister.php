<?php

namespace App\Listeners;

use App\Events\UserRegisterMailEvent;
use App\Mail\UserRegisterMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Mail;

class SendEmailNotificationUserRegister implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserRegisterMailEvent  $event
     * @return void
     */
    public function handle(UserRegisterMailEvent $event)
    {
        Mail::to($event->user)->send(new UserRegisterMail($event->user));
    }
}
