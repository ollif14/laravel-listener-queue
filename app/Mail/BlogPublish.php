<?php

namespace App\Mail;

use App\Models\blog;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class BlogPublish extends Mailable
{
    use Queueable, SerializesModels;
    protected $blog;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(blog $blog)
    {
        $this->blog = $blog;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('example@example.com')
                    ->view('send_email_blog_Publish')
                    ->with([
                        'pembuat' => $this->blog->pembuat,
                        'judul'=> $this->blog->judul,
                    ]);
    }
}
