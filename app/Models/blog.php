<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class blog extends Model
{
    protected $fillable = ['judul','pembuat','email','link','publish_status','publish_by'];

    public function user(){
        return $this->belongsTo(User::class);
    }
}
