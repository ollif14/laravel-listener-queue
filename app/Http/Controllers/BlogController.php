<?php

namespace App\Http\Controllers;

use App\Events\BlogPublishEvent;
use App\Events\BlogReviewEditorEvent;
use App\Events\BlogReviewEvent;
use App\Events\BlogReviewPembuatEvent;
use App\Http\Controllers\Controller;
use App\Http\Resources\BlogResource;
use App\Models\blog;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogs = blog::get();
        return BlogResource::collection($blogs);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => ['required'],
            'pembuat' => ['required'],
            'email' => ['email'],
            'link' => ['required'],
        ]);
        $blog = auth()->user()->blogs()->create($this->blogStore());
        $user_id = Auth()->user()->id;
        $user = User::where('id',$user_id)->first();

        event(new BlogReviewPembuatEvent($blog));
        event(new BlogReviewEditorEvent($user));

        return $blog;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(blog $blog)
    {
        return new BlogResource($blog);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $blog = blog::findOrFail($id);
        $request->validate([
            'judul' => ['required'],
            'pembuat' => ['required'],
            'email' => ['email'],
            'link' => ['required'],
        ]);

        $blog->update($this->blogStore());

        return new BlogResource($blog);
        
    }

    public function publish(Request $request, $id)
    {
        $blog = blog::findOrFail($id);
        $blog->update($this->publishStore());

        event(new BlogPublishEvent($blog));

        return new BlogResource($blog);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(blog $blog)
    {
        $blog->delete();
        return response()->json('Blog Berhasil Dihapus', 200);
    }

    public function blogStore(){
        return [
            'judul' => request('judul'),
            'pembuat' => request('pembuat'),
            'email' => request('email'),
            'link' => request('link'),
        ];
    }

    public function publishStore(){
        $userId=auth()->user()->id;
        return [
            'publish_status' => 1,
            'publish_by' => $userId,
        ];
    }
}
