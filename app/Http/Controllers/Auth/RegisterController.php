<?php

namespace App\Http\Controllers\Auth;

use App\Events\UserRegisterMailEvent;
use App\User;
use App\Http\Requests\RegisterRequest;
use App\Http\Controllers\Controller;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(RegisterRequest $request)
    {
        $user=User::create([
            'name' => request('name'),
            'username' => request('username'),
            'email' => request('email'),
            'password' => bcrypt(request('password')),
        ]);
        
        event(new UserRegisterMailEvent($user));

        return response('Thanks, You Are Register');
    }
}
