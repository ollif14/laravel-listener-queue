<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BlogResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'judul' => $this->judul,
            'pembuat' => $this->pembuat,
            'email' => $this->email,
            'link' => $this->link,
            'publish_status' => $this->publish_status,
            'publish_by' => $this->publish_by,
        ];
    }

    public function with($request)
    {
        return ['status' => 'Succes'];
    }
}
