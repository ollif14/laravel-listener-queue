<?php

Route::namespace('Auth')->group(function() {
    Route::post('login','LoginController');
    Route::post('logout','LogoutController');
    Route::post('register','RegisterController');
});
Route::middleware('auth:api')->group(function(){
    Route::post('create-blog', 'BlogController@store');
    Route::patch('update-blog/{id}', 'BlogController@update');
    Route::patch('publish-blog/{id}', 'BlogController@publish')->middleware('publish');
    Route::delete('delete-blog/{id}', 'BlogController@destroy');
});